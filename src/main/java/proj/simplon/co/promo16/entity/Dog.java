package proj.simplon.co.promo16.entity;

import java.sql.Date;

public class Dog {
    private int id;
    private String breed;
    private String first_name;
    private Date birthDate;

    public Dog() {
    }

    public Dog(int id, String breed, String first_name, Date birthDate) {
        this.id = id;
        this.breed = breed;
        this.first_name = first_name;
        this.birthDate = birthDate;
    }

    public Dog(String breed, String first_name, Date birthDate) {
        this.breed = breed;
        this.first_name = first_name;
        this.birthDate = birthDate;
    }

    public String getBreed() {
        return breed;
    }

    public void setBreed(String breed) {
        this.breed = breed;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
