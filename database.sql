CREATE TABLE IF NOT EXISTS dog (
  id INTEGER PRIMARY KEY AUTO_INCREMENT NOT NULL,
  breed VARCHAR(50),
  first_name VARCHAR(50),
  birthdate DATE
) ENGINE = InnoDB DEFAULT CHARSET = utf8;
-- DROP TABLE IF EXISTS dog;
INSERT INTO
  dog (breed, first_name, birthdate)
VALUES
  ("Shitzu", "Popoye", "2016/12/01"),
  ("Berger allemand", "Guesh", "1991/09/14");